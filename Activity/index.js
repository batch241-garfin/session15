

// Objective 1

let firstName = "Romhelson";
console.log("My First Name: " + firstName);

let lastName = "Garfin";
console.log("Last Name: " + lastName);

let age = 23;
console.log("Age: " + age);

let hobbies = ["Singing", "Drawing", "Watching Series"];
console.log("Hobbies: ");
console.log(hobbies);

const work =
{
	houseNumber: "10B",
	street: "Sweet Sultan",
	city: "Los Angeles",
	state: "California"
}
console.log("Work Address: ");
console.log(work);


// Objective 2

let fullName = "Steve Rogers";
console.log("My full name is: " + fullName);

let age1 = 40;
console.log("My current age is: " + age1);
	
let friends = ["Tony ","Bruce ", "Thor", "Natasha" ,"Clint","Nick"];
console.log("My Friends are: ");
console.log(friends);

let profile =
{
	userName: "captain_america",
	fullName: "Steve Rogers",
	age: 40,
	isActive: false,
}

console.log("My Full Profile: ")
console.log(profile);

fullName = "Bucky Barnes";
console.log("My bestfriend is: " + fullName);

const lastLocation = "Arctic Ocean";
// lastLocation = "Atlantic Ocean";
console.log("I was found frozen in: " + lastLocation);