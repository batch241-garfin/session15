console.log("Hello World!");

// Syntax And Statements //
	// Are instructions that we tell the computer
	// to perform. JS Statements usually ends
	// with semicolon

	// A Syntax is a set of rules that we describes
	// statements must be constructed.

	// All lines/ blocks of code should be written
	// in specifc manner. This is due to how
	// these codes where initially programmed to
	// fnuction in a certain manner.

// Variables //

	// Used to contain data. Any information
	// that is used by an application is stored
	// in what we call the memory.
	// When we create variables, certain portions
	// of device's memory is given a name that
	// we call variables.

	// Declaring Variables:
		// Let/ const variableName = Value;

	let myVariable = "Ada LOvelace";
	const constVariable = "John Doe";
	console.log(myVariable);

	// console.log() prints values of variables
	// let is used when the value of the
	// 	variable changes.
	// const is used when the value of the
	// 	variable wont change. 

// Guides in Writing Variables //

	// 1. Use the let keyword followed the
		// variable name of your choice and use the
		// assignment operator (=) to assign the 
		// value.

	// 2. Vriable names should start with lowercase
		// character, use camelCasing for the
		// multiple words.

	// 3. For const variables, use the const
	 	// keyword.

	// 4. Variable names should be indicative of
		// the value being stored to avoid
		// confusion.

//	Declare and Initialize	//

	// Initializing variables - the instance
	// when a variable is given its first/ initial
	// value

		// Syntax:
			// let/ const variableName = initial;

	let productName = "desktop computer";
	console.log(productName);

	// Reassigning Value //

	productName = "Personal Computer";
	console.log(productName);

	const name = "Romhel";
	console.log(name);
	// name = "Mhel";
	// console.log(name);

	// var keyword is an ecmaScript1 feature (1997).

// Scope of Let/ Const vs. Local/ Global Scope

	// Scope essentially means where these variables for
	// use.
	// let/const are block scope.
		// A block is a chunk of code bounded by {}.

	let outerVariable ="Hello";
	{
		let innerVariable ="Hello Again";
		console.log(innerVariable);
	}
	console.log(outerVariable);

	const outerVariable1 ="Hello";
	{
		const innerVariable1 ="Hello Again";
		console.log(innerVariable1);
	}
	console.log(outerVariable1);

//	Multiple Variable Declarations	//

	let productCode ="DC017", productBrand ="Dell";
	console.log(productCode);
	console.log(productBrand);

// Different Data Types of Javascript	//

	// 1. Strings are series of characters that create
		// a word, a phrase, a sentence or anything
		// related to creating a text.
		// Strings in JS can be written either a
		// single('') or double ("") quote.

	let country = "Philippines";
	let province = 'Metro Manila';

	console.log(country);
	console.log(province);

		// Concatenate Strings
			// Multiple string values can be combined
			// to create a single string using
			// the "+" symbol

		let fullAddress = province + ', ' + country;
		console.log(fullAddress);

		// String Using Escape Character

		let message = 'John\'s emplyees went home early';
		console.log(message);

		let message1 = "John's emplyees\n went home early";
		console.log(message1);

	// 2. Numbers (Whole Numbers, Decimals, Exponents) 

	let headcount = 27; 
	console.log(typeof headcount);
	console.log(headcount);

	let grade = 98.7;
	console.log(grade);

	let planetDistance = 2e10;
	console.log(planetDistance);

	console.log("jonh's grade last quarter is " + grade);

	// 3. Boolean values are normally used to store values
		// relating to center things. Either True or False.

	let isMarried = "false";
	let inGoodConduct = "true";
	console.log(isMarried);
	console.log(inGoodConduct);

	// 4. Arrays are a special kind of data type that is
		// use to store multiple values.

		// Syntax: let/const arrayName = [elemA, elemB];

	let grades = [98,92.1,90.1,94.7];
	console.log(grades);

	let details = ["John",32,true];
	console.log(details);

	// 5. Objects are another special kind of data type
		// that is used to mimic the real world objects or
		// items.

		// Syntax: let/const objectName = 
			// {
			// 	propertyA: value,
			// 	propertyB: value
			// }

	let person =
	{
		firstName: "John",
		lastName: "Smith",
		age: 32,
		isMarried: false,
		contact: ["+6391234567891", "8123-2345"],
		address:
		{
			houseNumber: "345",
			city: "Manila"
		}
	}
	console.log(person);

		// Constant Objects and Arrays //

			// We can change the element of an array to a 
			// constant variable.

		const anime = ["one piece", "one punch man", "your lie in april"];
		console.log(anime);
		anime[0] = 'kimetsu no yaba';
		console.log(anime);


	// 6. Null is used to intentionally express the absence
		// of a variable declaration/ initialization.

	let spouse = null;
	console.log(spouse);

	// 7. Undefined represents the state of a variable that
		//has been declared but without an assigned value

	let inARelationship;
	console.log(inARelationship);




